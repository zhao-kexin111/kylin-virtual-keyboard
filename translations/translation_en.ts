<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>VirtualKeyboardEntryManager</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="78"/>
        <source>Disable the float button</source>
        <translation>Disable the float ball</translation>
    </message>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="85"/>
        <source>Enable the float button</source>
        <translation>Enable the float ball</translation>
    </message>
</context>
<context>
    <name>VirtualKeyboardTrayIcon</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardtrayicon.cpp" line="25"/>
        <source>kylin-virtual-keyboard</source>
        <translation>Kylin Virtual Keyboard</translation>
    </message>
</context>
</TS>
