<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>VirtualKeyboardEntryManager</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="78"/>
        <source>Disable the float button</source>
        <translation>禁用键盘悬浮球</translation>
    </message>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardentrymanager.cpp" line="85"/>
        <source>Enable the float button</source>
        <translation>启用键盘悬浮球</translation>
    </message>
</context>
<context>
    <name>VirtualKeyboardTrayIcon</name>
    <message>
        <location filename="../src/virtualkeyboardentry/virtualkeyboardtrayicon.cpp" line="25"/>
        <source>kylin-virtual-keyboard</source>
        <translation>麒麟虚拟键盘</translation>
    </message>
</context>
</TS>
